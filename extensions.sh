#!/bin/bash

# curl -s https://raw.githubusercontent.com/clyfe/vscode/master/extensions.sh | /bin/bash

extensions=(
betterthantomorrow.calva
# clyfe.calva-extras
)

for i in ${extensions[@]}; do
  code --install-extension $i
done
