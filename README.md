## VSCode Clojure Config for Paredit Newbies

### About

A transitional config towords paredit based on [Calva](https://calva.io).  
Requires [Calva Extras](https://github.com/clyfe/calva-extras).

Transitional because:
* It does not steal your keyboard completley with foreign #$%&!? commands.
* But it does try to move you closer to paredit use.

### Install

```bash
cd ~/.config/Code/User
git init
git remote add origin git@github.com:clyfe/vscode.git
git pull origin master
bash extensions.sh
```

### Usage

* **Paredit s-expr movement** via `Ctrl+Arrows`.
* **Normal word selection** via `Ctrl+Shift+Arrows`. Use this for movement
  fallback (with subsequent deselect) when:
  * paredit movement gets frustrating,
  * you want to move by word (into namespaced symbols or keywords for example).
* `Alt+D` **paredit kill to clipboard** the one true s-expr command:
  * (Operates on sexps after cursor.)
  * Deletes (but overwrites clipbord).
  * Cuts.
  * Copies (extra undo needed!).
* **The new new `backspace`**:
  * Kills multiple spaces and takes stops directy in the end position.
  * No more delete "space by space" timewaste.
  * Takes stops to all positions of interest.

### License

Copyright © 2019 clyfe
Distributed under the MIT license.
